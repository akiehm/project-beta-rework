import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './pages/ManufacturerList';
import ManufacturerForm from './pages/ManufacturerForm';
import ModelsList from './pages/ModelsList';
import BicycleList from './pages/BicycleList';
import ModelForm from './pages/ModelForm';
import BicycleForm from './pages/BicycleForm';
import SalesPersonForm from './pages/salesPages/SalesPersonForm';
import CustomerForm from './pages/salesPages/CustomerForm';
import TechnicianForm from './pages/servicePages/TechnicianForm';
import AppointmentForm from './pages/servicePages/AppointmentForm';
import AppointmentList from './pages/servicePages/AppointmentList';
import SerialHistory from './pages/servicePages/SerialHistory';
import SalesRecordList from './pages/salesPages/SalesRecordList';
import SalesRecordForm from './pages/salesPages/SalesRecordForm';
import SaleHistory from './pages/salesPages/SaleHistory';
import Profile from './components/Profile';
import './index.css';


function App(props) {
  return (
    <>
        <Nav />
        <div>
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path='manufacturers' element= {<ManufacturerList manufacturers={props.manufacturers}/>}/>
            <Route path='manufacturers/new' element= {<ManufacturerForm/>}/>
            <Route path='bicycles' element= {<BicycleList automobiles={props.bicycles}/>}/>
            <Route path="models">
              <Route path="" element={<ModelsList models={props.models}/>} />
              <Route path="new" element={<ModelForm />} />
            </Route>
            <Route path='bicycles/new' element={<BicycleForm/>}/>
            <Route path='salespeople' element={<SalesPersonForm/>}/>
            <Route path='customers' element={<CustomerForm/>}/>
            <Route path='salesrecords' element={<SalesRecordList salesrecords={props.salesrecords}/>}/>
            <Route path='salesrecords/new' element={<SalesRecordForm/>}/>
            <Route path='salehistory' element={<SaleHistory salesrecords={props.salesrecords}/>}/>
            <Route path='technicians/new' element={<TechnicianForm/>}/>
            <Route path='appointments' element={<AppointmentList appointments={props.appointments}/>}/>
            <Route path='appointments/new' element={<AppointmentForm/>}/>
            <Route path='search' element={<SerialHistory appointments={props.appointments}/>}/>
            <Route path='profile' element={<Profile/>}/>
          </Routes>
        </div>
    </>
  );
}

export default App;
