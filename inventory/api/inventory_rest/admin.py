from django.contrib import admin
from .models import Manufacturer, BicycleModel, Bicycle


admin.site.register(Bicycle)
admin.site.register(Manufacturer)
admin.site.register(BicycleModel)
