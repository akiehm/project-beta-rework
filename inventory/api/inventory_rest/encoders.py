from common.json import ModelEncoder

from .models import Bicycle, Manufacturer, BicycleModel


class ManufacturerEncoder(ModelEncoder):
    model = Manufacturer
    properties = [
        "id",
        "name",
    ]


class BicycleModelEncoder(ModelEncoder):
    model = BicycleModel
    properties = [
        "id",
        "name",
        "picture_url",
        "manufacturer",
    ]
    encoders = {
        "manufacturer": ManufacturerEncoder(),
    }


class BicycleEncoder(ModelEncoder):
    model = Bicycle
    properties = [
        "id",
        "color",
        "year",
        "serial",
        "model",
    ]
    encoders = {
        "model": BicycleModelEncoder(),
    }
