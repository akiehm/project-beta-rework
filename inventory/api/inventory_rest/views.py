from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    BicycleEncoder,
    ManufacturerEncoder,
    BicycleModelEncoder,
)
from .models import Bicycle, Manufacturer, BicycleModel


@require_http_methods(["GET", "POST"])
def api_bicycles(request):
    if request.method == "GET":
        bikes = Bicycle.objects.all()
        return JsonResponse(
            {"bikes": bikes},
            encoder=BicycleEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            model_id = content["model_id"]
            model = BicycleModel.objects.get(pk=model_id)
            content["model"] = model
            bike = Bicycle.objects.create(**content)
            return JsonResponse(
                bike,
                encoder=BicycleEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the bicycle"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_bicycle(request, serial):
    if request.method == "GET":
        try:
            bike = Bicycle.objects.get(serial=serial)
            return JsonResponse(
                bike,
                encoder=BicycleEncoder,
                safe=False
            )
        except Bicycle.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            bike = Bicycle.objects.get(serial=serial)
            bike.delete()
            return JsonResponse(
                bike,
                encoder=BicycleEncoder,
                safe=False,
            )
        except Bicycle.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            bike = Bicycle.objects.get(serial=serial)

            props = ["color", "year"]
            for prop in props:
                if prop in content:
                    setattr(bike, prop, content[prop])
            bike.save()
            return JsonResponse(
                bike,
                encoder=BicycleEncoder,
                safe=False,
            )
        except Bicycle.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_manufacturers(request):
    if request.method == "GET":
        manufacturers = Manufacturer.objects.all()
        return JsonResponse(
            {"manufacturers": manufacturers},
            encoder=ManufacturerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            manufacturer = Manufacturer.objects.create(**content)
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the manufacturer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_manufacturer(request, pk):
    if request.method == "GET":
        try:
            manufacturer = Manufacturer.objects.get(id=pk)
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False
            )
        except Manufacturer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            manufacturer = Manufacturer.objects.get(id=pk)
            manufacturer.delete()
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False,
            )
        except Manufacturer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            manufacturer = Manufacturer.objects.get(id=pk)

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(manufacturer, prop, content[prop])
            manufacturer.save()
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False,
            )
        except Manufacturer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_bicycle_models(request):
    if request.method == "GET":
        models = BicycleModel.objects.all()
        return JsonResponse(
            {"models": models},
            encoder=BicycleModelEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            manufacturer_id = content["manufacturer_id"]
            manufacturer = Manufacturer.objects.get(id=manufacturer_id)
            content["manufacturer"] = manufacturer
            model = BicycleModel.objects.create(**content)
            return JsonResponse(
                model,
                encoder=BicycleModelEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the model"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_bicycle_model(request, pk):
    if request.method == "GET":
        try:
            model = BicycleModel.objects.get(id=pk)
            return JsonResponse(
                model,
                encoder=BicycleModelEncoder,
                safe=False
            )
        except BicycleModel.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            model = BicycleModel.objects.get(id=pk)
            model.delete()
            return JsonResponse(
                model,
                encoder=BicycleModelEncoder,
                safe=False,
            )
        except BicycleModel.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            model = BicycleModel.objects.get(id=pk)
            props = ["name", "picture_url"]
            for prop in props:
                if prop in content:
                    setattr(model, prop, content[prop])
            model.save()
            return JsonResponse(
                model,
                encoder=BicycleModelEncoder,
                safe=False,
            )
        except BicycleModel.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
