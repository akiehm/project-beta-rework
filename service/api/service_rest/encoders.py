from common.json import ModelEncoder
from .models import BicycleVO, Technician, Appointment


class BicycleVOEncoder(ModelEncoder):
    model = BicycleVO
    properties=['serial']

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [ 'id','name','employee_number']


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ['id','serial','owner','date','time','reason','technician','is_vip','is_finished']
    encoders ={
        "technician": TechnicianEncoder(),
    }

    def get_extra_data(self,o):
        return {"technician": o.technician.name}

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = ['id','serial','owner','date','time','reason','technician','is_vip','is_finished']
    encoders ={
        "technician": TechnicianEncoder()
    }

    def get_extra_data(self,o):
        return {"technician": o.technician.name}