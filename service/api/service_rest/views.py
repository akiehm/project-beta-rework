from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import BicycleVO, Technician, Appointment
from .encoders import BicycleVOEncoder, TechnicianEncoder, AppointmentDetailEncoder, AppointmentListEncoder
import json

# Create your views here.
@require_http_methods(["GET","POST"])
def api_list_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )

@require_http_methods(["GET","POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.get(id=content["technician"])
        content["technician"] = technician
        try:
            serial=content["serial"]
            BicycleVO.objects.get(serial=serial)
            content["is_vip"] = True
        except BicycleVO.DoesNotExist:
            content["is_vip"] = False
        
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
          appointment,
          encoder=AppointmentListEncoder,
          safe=False
        )

@require_http_methods(["DELETE","GET","PUT"])
def api_show_appointment(request,pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment, encoder=AppointmentDetailEncoder, safe=False
        )
    
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse( appointment, encoder=AppointmentDetailEncoder, safe=False,)
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        content = json.loads(request.body)
        appointment = Appointment.objects.get(id=pk)
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )

    
