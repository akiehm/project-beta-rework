import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest.models import BicycleVO

def get_bicycle():
    response = requests.get("http://inventory-api:8000/api/bicycles/")
    content = json.loads(response.content)
    for bike in content["bikes"]:
        BicycleVO.objects.update_or_create(
            import_href=bike["href"],
            defaults={
                "serial": bike["serial"],
            },
        )

def poll():
    while True:
        print('Service poller polling for data')
        try:
            get_bicycle()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
